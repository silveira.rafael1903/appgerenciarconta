﻿using FinanceiroDominio.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebAppHelper.Models
{
    public class DadosBasicosModel
    {

        /// <summary>
        /// Nome
        /// </summary>
        public String Nome { get; set; }


        /// <summary>
        /// SaldoAtual
        /// </summary>
        public decimal SaldoAtual { get; set; }

        /// <summary>
        /// Movimentos
        /// </summary>
        public List<DadosMovimentacao> Movimentos { get; set; }
    }

    public class DadosMovimentacao
    {
        /// <summary>
        /// Saldo
        /// </summary>
        public decimal Saldo { get; set; }

        /// <summary>
        /// DataMovimento
        /// </summary>
        public DateTime DataMovimento { get; set; }

        /// <summary>
        /// TipoMovimento
        /// </summary>
        public string TipoMovimento { get; set; }
    }
}