﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebAppHelper.Models
{
    public class SimularParcelamentoReq
    {
        /// <summary>
        /// valorCompra
        /// </summary>
        public double ValorCompra { get; set; }

        /// <summary>
        /// QuantidadeDeParcelas
        /// </summary>
        public int QuantidadeDeParcelas { get; set; }

        /// <summary>
        /// TaxaJuros
        /// </summary>
        public double TaxaJuros { get; set; }

        /// <summary>
        /// DataCompra
        /// </summary>
        public DateTime DataCompra { get; set; }

        /// <summary>
        /// DataCorteFatura
        /// </summary>
        public DateTime DataCorteFatura { get; set; }
        /// <summary>
        /// qtdDiasPrimeiroVenc trata-se da carencia , default 30
        /// </summary>
        public int QtdDiasPrimeiroVenc { get; set; }

        /// <summary>
        /// txIOFDia
        /// </summary>
        public double TxIOFDia { get; set; }

        /// <summary>
        /// txIOFFixa
        /// </summary>
        public double txIOFFixa { get; set; }


        /// <summary>
        /// Flagposproximovencimento
        /// </summary>
        public int Flagposproximovencimento { get; set; }
    }
}