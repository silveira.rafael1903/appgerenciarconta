﻿using FinanceiroDominio;
using FinanceiroDominio.DTO;
using System;
using System.Web.Http;

namespace WebAppHelper.Controllers
{
    public class ContaController : ApiController
    {
        /// <summary>
        /// Realiza um deposito para a conta 
        /// </summary>
        /// <remarks>
        /// Exemplo de entrada:
        /// 
        ///     {
        ///         "cpf": 95782872080,
        ///         "valor": 100.00
        ///        
        ///     }
        /// </remarks>
        [Route("RealizarDeposito")]
        public void RealizarDeposito(string cpf, decimal valor)
        {
            var contaDm = new ContaDominio();
            var id_Conta = contaDm.BuscarId_Conta(cpf);
            contaDm.InserirTransacao(CriarTransacao(id_Conta, valor, TipoTransacaoEnum.Deposito));
        }


        /// <summary>
        /// Realiza um saque para a conta 
        /// </summary>
        /// <remarks>
        /// Exemplo de entrada:
        /// 
        ///     {
        ///         "cpf": 95782872080,
        ///         "valor": 100.00
        ///        
        ///     }
        /// </remarks>
        [Route("RealizarSaque")]
        public void RealizarSaque(string cpf, decimal valor)
        {
            var contaDm = new ContaDominio();
            var id_Conta = contaDm.BuscarId_Conta(cpf);
            contaDm.InserirTransacao(CriarTransacao(id_Conta, valor, TipoTransacaoEnum.Saque));
        }


        private TransacaoDTO CriarTransacao(int id_Conta, decimal valor, TipoTransacaoEnum tpTrans)
        {
            return new TransacaoDTO() { Id_Conta = id_Conta, DataHora = DateTime.Now, TipoTransacao = (int)tpTrans, Valor = valor };
        }
    }
}
