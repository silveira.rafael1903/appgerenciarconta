﻿
using FinanceiroDominio;
using FinanceiroDominio.DTO;
using System.Collections.Generic;
using System.Web.Http;
using WebAppHelper.Models;


namespace WebAppHelper.Controllers
{
    public class ClienteController : ApiController
    {
        /// <summary>
        /// Insere um cliente na base
        /// </summary>
        /// <remarks>
        /// Exemplo de entrada:
        /// 
        ///     {
        ///         "cpf": 85964560075,
        ///         "nome": john doe

        ///        
        ///     }
        /// </remarks>
        [Route("Criar")]
        public void Criar(string cpf,
            string nome
            )
        {
            var cdm = new ClienteDominio();
            var cliente = new ClienteDTO()
            {
                CPF = cpf,
                Nome = nome
            };
            cdm.InserirCliente(cliente);
        }

        /// <summary>
        /// Obtem dados basicos da conta o cliente 
        /// </summary>
        /// <remarks>
        /// Exemplo de entrada:
        /// 
        ///     {
        ///         "cpf": id_conta
        ///     }
        /// </remarks>
        [Route("ObterDashBoard")]

        public DadosBasicosModel ObterDashBoard(string cpf)
        {
            var contaDm = new ContaDominio();
            var clienteDM = new ClienteDominio();

            var id_Conta = contaDm.BuscarId_Conta(cpf);
            var historicos = contaDm.ObterLimitesHistorico(id_Conta);

            var rt = new DadosBasicosModel();

            var cliente = clienteDM.BuscarCliente(cpf);
            rt.Nome = cliente.Nome;

            rt.SaldoAtual = historicos.Saldo;
            rt.Movimentos = ConverterListaHistorico(historicos.Transacoes);

            return rt;
        }


        /// <summary>
        /// Obtem dados basicos do cliente 
        /// </summary>
        /// <remarks>
        /// Exemplo de entrada:
        /// 
        ///     {
        ///         "cpf": 85964560075     
        ///     }
        /// </remarks>
        [Route("ObterCliente")]
        public ClienteDTO ObterCliente(string cpf)
        {
            var cdm = new ClienteDominio();

            var rt = cdm.BuscarCliente(cpf);

            return rt;
        }


        private List<DadosMovimentacao> ConverterListaHistorico(List<TransacaoDTO> historicos)
        {
            var listaRt = new List<DadosMovimentacao>();
            foreach (var item in historicos)
            {
                var mov = new DadosMovimentacao();
                mov.Saldo = item.Valor;
                mov.DataMovimento = item.DataHora;
                var enumVal = (TipoTransacaoEnum)item.TipoTransacao;
                mov.TipoMovimento = enumVal.ObterdescricaoEnum();

                listaRt.Add(mov);
            }

            return listaRt;
        }
    }
}
