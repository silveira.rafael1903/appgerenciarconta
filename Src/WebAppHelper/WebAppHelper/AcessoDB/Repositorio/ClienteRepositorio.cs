﻿using AcessoDB.Model;
using System;
using System.Collections.Generic;
using System.Linq;


namespace AcessoDB.Repositorio
{
    public class ClienteRepositorio
    {

        public void InserirCliente(string cpf, string nome)
        {
            try
            {
                var cliente = new Cliente() { CPF = cpf, Nome = nome };
                cliente.Contas = new List<Conta>() { new Conta() { SaldoAtual = 0 } };

                using (var context = new WarrenDBEntities())
                {
                    context.Clientes.Add(cliente);
                    context.SaveChanges();
                }
            }
            catch (Exception e)
            {

                throw e;
            }


        }

        public Cliente BuscarCliente(string cpf)
        {
            Cliente c = null;
            using (var context = new WarrenDBEntities())
            {
                c = context.Clientes.Where(x => x.CPF == cpf).FirstOrDefault();
            }
            return c;
        }
    }
}
