﻿using AcessoDB.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AcessoDB.Repositorio
{
    public class ContaRepositorio
    {
        public void InserirTransacaoPositiva(Transacao transacao)
        {
            transacao.Valor = transacao.Valor * 1;
            InserirTransacaoInternal(transacao);
        }

        public void InserirTransacaoNegativa(Transacao transacao)
        {
            transacao.Valor = transacao.Valor * -1;
            InserirTransacaoInternal(transacao);
        }

        public List<Transacao> ObterTransacoes(int id_conta)
        {
            using (var context = new WarrenDBEntities())
            {
                return context.Transacaos.Where(x => x.Id_Conta == id_conta).ToList();
            }
        }

        public bool ContaExiste(int id_conta)
        {
            using (var context = new WarrenDBEntities())
            {
                Conta conta = context.Contas.Where(x => x.Id_Conta == id_conta).FirstOrDefault();

                return conta != null;
            }
        }

        public bool PossuiLimite(int id_conta, decimal valor)
        {
            using (var context = new WarrenDBEntities())
            {
                Conta conta = context.Contas.Where(x => x.Id_Conta == id_conta).FirstOrDefault();

                return conta.SaldoAtual - valor >= 0;
            }

        }

        public int BuscarId_conta(int id_cliente)
        {
            using (var context = new WarrenDBEntities())
            {
                Conta conta = context.Contas.Where(x => x.Id_Cliente == id_cliente).FirstOrDefault();

                return conta.Id_Conta;
            }
        }

        private void InserirTransacaoInternal(Transacao transacao)
        {
            using (var context = new WarrenDBEntities())
            {
                Conta conta = context.Contas.Where(x => x.Id_Conta == transacao.Id_Conta).FirstOrDefault();

                conta.SaldoAtual = conta.SaldoAtual + transacao.Valor.Value;

                if (conta.Transacoes == null)
                {
                    conta.Transacoes = new List<Transacao>();
                }

                conta.Transacoes.Add(transacao);

                context.SaveChanges();
            }
        }
    }
}
