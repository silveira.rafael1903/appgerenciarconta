﻿using System;
using System.Collections.Generic;
using System.ComponentModel;

using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace FinanceiroDominio
{
    public static class Utilitarios
    {

        // facilitar a tramsferencia de dados entre dtos e classes de dominio
        // exemplo de uso:  ClasseNova hcDTO = Utilitarios.ConverterPara<ClasseNova>(ClasseAntiga);
        public static T ConverterPara<T>(object input)
        {
            Type tipo = typeof(T);
            Type tipoIn = input.GetType();

            T instance = (T)Activator.CreateInstance(tipo);

            PropertyInfo[] propInfo = input.GetType().GetProperties();

            foreach (var item in propInfo)
            {
                try
                {

                    var propNew = instance.GetType().GetProperty(item.Name);

                    if (propNew != null && propNew.CanWrite)
                    {

                        var itemValue = item.GetValue(input, null);

                        if (itemValue != null)
                        {
                            var t = Nullable.GetUnderlyingType(item.PropertyType) ?? item.PropertyType;
                            var safeValue = (itemValue == null) ? null : Convert.ChangeType(itemValue, t);

                            var t2 = Nullable.GetUnderlyingType(propNew.PropertyType) ?? propNew.PropertyType;

                            if (t.Name == t2.Name)
                            {
                                propNew.SetValue(instance, safeValue, null);
                            }
                        }
                    }


                }
                catch (Exception ex)
                {

                    throw ex;
                }
            }

            return instance;

        }

        //busca description de enum

        public static string ObterdescricaoEnum(this System.Enum valor)
        {
            Type tipo = valor.GetType();

            string nome = System.Enum.GetName(tipo, valor);

            if (nome != null)
            {
                FieldInfo campo = tipo.GetField(nome);

                if (campo != null)
                {
                    DescriptionAttribute attr = Attribute.GetCustomAttribute(campo,
                        typeof(DescriptionAttribute)) as DescriptionAttribute;

                    if (attr != null)
                    {
                        return attr.Description;
                    }
                }

            }

            return null;
        }

    }
}
