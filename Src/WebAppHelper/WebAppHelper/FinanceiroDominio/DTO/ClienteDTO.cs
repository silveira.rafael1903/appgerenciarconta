﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinanceiroDominio.DTO
{
    public class ClienteDTO
    {
        public int Id_Cliente { get; set; }

        public string CPF { get; set; }

        public string Nome { get; set; }
    }
}
