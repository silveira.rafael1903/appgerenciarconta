﻿using System.Collections.Generic;
using System.Linq;

namespace FinanceiroDominio.DTO
{
    public class HistoricoContaDTO
    {
        public int Id_Conta
        {
            get
            {
                int value = 0;

                if (Transacoes.FirstOrDefault() != null)
                {
                    value = Transacoes.FirstOrDefault().Id_Conta;
                }


                return value;
            }
        }

        public decimal Saldo
        {
            get
            {
                decimal value = 0;

                if (Transacoes != null && Transacoes.Count() > 0)
                {
                    value = Transacoes.Sum(x => x.Valor);
                }

                return value;
            }
        }

        public List<TransacaoDTO> Transacoes { get; set; }
    }
}
