﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinanceiroDominio.DTO
{
    public enum TipoTransacaoEnum
    {
        [Description("Saque")]
        Saque = 1,
        [Description("Deposito")]
        Deposito = 2
    }
}
