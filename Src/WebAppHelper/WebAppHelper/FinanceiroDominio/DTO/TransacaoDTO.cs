﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinanceiroDominio.DTO
{
    public class TransacaoDTO
    {
        public DateTime DataHora { get; set; }

        public decimal Valor { get; set; }

        public int Id_Conta { get; set; }

        public int TipoTransacao { get; set; }
    }
}
