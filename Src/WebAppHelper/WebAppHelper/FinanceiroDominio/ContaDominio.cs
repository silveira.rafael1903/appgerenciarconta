﻿using AcessoDB.Model;
using AcessoDB.Repositorio;
using FinanceiroDominio.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinanceiroDominio
{
    public class ContaDominio
    {
        public void InserirTransacao(TransacaoDTO transacao)
        {
            var contaAcessoDB = new ContaRepositorio();

            if (!contaAcessoDB.ContaExiste(transacao.Id_Conta))
            {
                throw new Exception("A conta não existe.");
            }

            switch ((TipoTransacaoEnum)transacao.TipoTransacao)
            {
                case TipoTransacaoEnum.Deposito:
                    contaAcessoDB.InserirTransacaoPositiva(CriarTransacao(transacao));
                    break;
                case TipoTransacaoEnum.Saque:
                    if (contaAcessoDB.PossuiLimite(transacao.Id_Conta, transacao.Valor))
                    {
                        contaAcessoDB.InserirTransacaoNegativa(CriarTransacao(transacao));
                    }
                    else
                    {
                        throw new Exception("Não há limite para a operação solicitada.");
                    }
                    break;
            }
        }

        public int BuscarId_Conta(string cpf)
        {

            var clienteDominio = new ClienteDominio();

            var cliente = clienteDominio.BuscarCliente(cpf);

            var contaAcessoDB = new ContaRepositorio();

            return contaAcessoDB.BuscarId_conta(cliente.Id_Cliente);

        }

        public HistoricoContaDTO ObterLimitesHistorico(int id_conta)
        {
            var contaAcessoDB = new ContaRepositorio();

            var transacoes = contaAcessoDB.ObterTransacoes(id_conta);

            var rt = ConverterListaHistorico(transacoes);

            return rt;
        }

        private HistoricoContaDTO ConverterListaHistorico(List<Transacao> lt)
        {
            var rt = new HistoricoContaDTO();
            rt.Transacoes = new List<TransacaoDTO>();

            foreach (var item in lt)
            {
                TransacaoDTO hcDTO = Utilitarios.ConverterPara<TransacaoDTO>(item);

                rt.Transacoes.Add(hcDTO);
            }

            return rt;
        }

        private Transacao CriarTransacao(TransacaoDTO transacao)
        {
            return new Transacao()
            {
                DataHora = transacao.DataHora,
                TipoTransacao = (int)transacao.TipoTransacao,
                Valor = transacao.Valor,
                Id_Conta = transacao.Id_Conta
            };
        }
    }
}
