﻿using AcessoDB.Repositorio;
using FinanceiroDominio.DTO;
using System;

namespace FinanceiroDominio
{
    public class ClienteDominio
    {
        public void InserirCliente(ClienteDTO paramIN)
        {
            var clienteAcessoDB = new ClienteRepositorio();

            var clienteJaInserido = clienteAcessoDB.BuscarCliente(paramIN.CPF);

            if (clienteJaInserido != null)
            {
                throw new Exception("Cliente já existente.");
            }

            clienteAcessoDB.InserirCliente(paramIN.CPF, paramIN.Nome);
        }

        public ClienteDTO BuscarCliente(string cpf)
        {
            var clienteAcessoDB = new ClienteRepositorio();
            ClienteDTO rt = null;

            var c = clienteAcessoDB.BuscarCliente(cpf);

            if (c != null)
            {
                rt = new ClienteDTO()
                {
                    CPF = c.CPF,
                    Id_Cliente = c.Id_Cliente,
                    Nome = c.Nome                    
                };
            }
            else
            {
                throw new Exception("Cliente não encontrado.");
            }

            return rt;
        }
    }
}
