--drop table  Cliente
create table Cliente (
Id_Cliente int identity not null primary key,
CPF varchar(11) not null,
Nome varchar(50) not null
)

create table Conta (
Id_Conta int identity not null primary key,
SaldoAtual money not null,
Id_Cliente int  not null,
FOREIGN KEY (Id_Cliente) REFERENCES Cliente(Id_Cliente)
)

---drop table  Transacoes
create table Transacoes(
Id_transacao int identity not null primary key,
TipoTransacao int not null ,
DataHora datetime not null,
Id_Conta int not null,
Valor money ,
FOREIGN KEY (Id_Conta) REFERENCES Conta(Id_Conta)
)
